let cloud;
let vaseknap;
let drikkeglasknap;
let kopperknap;
let skyknap;
let bjergknap;
let brugsknap;
let lampeknap;
let kontaktknap;
let vaseimg,vaseimg1,vaseimg2;
let hoeje,lilglas,lilglas1,striber,striber1,striber2,vinglas;
let avokopper,kopper;
let sky,skyer,skyer1,skyer2,skyer3;
let skaale,isbjoern,isbjoern1,holdere,hjerne,hjerne1,fed,hund,isbjoern2;
let bjerg,bjerg3d1,bjergglasur,bjergproces;
let lampeblaa,lampeblaa1,lampehvid,lampehvid1;
let vt,vt1,vt2;

function preload(){
  vaseimg=createImg('vaerker/vase.jpg');
  vaseimg.hide();
  vaseimg1=createImg('vaerker/vase1.jpg');
  vaseimg1.hide();
  vaseimg2=createImg('vaerker/vase2.jpg');
  vaseimg2.hide();
  hoeje=createImg('vaerker/hoeje.jpg');
  hoeje.hide();
  lilglas=createImg('vaerker/lilglas.jpg');
  lilglas.hide();
  striber=createImg('vaerker/striber.jpg');
  striber.hide();
  vinglas=createImg('vaerker/vinglas.jpg');
  vinglas.hide();
  lilglas1=createImg('vaerker/lilglas1.jpg');
  lilglas1.hide();
  striber1=createImg('vaerker/striber1.jpg');
  striber1.hide();
  striber2=createImg('vaerker/striber2.jpg');
  striber2.hide();
  avokopper=createImg('vaerker/avokopper.jpg');
  avokopper.hide();
  kopper=createImg('vaerker/kopper.jpg');
  kopper.hide();
  sky=createImg('vaerker/sky1.png');
  sky.hide();
  skyer1=createImg('vaerker/skyer1.jpg');
  skyer1.hide();
  skyer=createImg('vaerker/skyer.jpg');
  skyer.hide();
  skyerbord=createImg('vaerker/skyerbord.jpg');
  skyerbord.hide();
  skyer3=createImg('vaerker/skyer3.jpg');
  skyer3.hide();
  skyer2=createImg('vaerker/skyer2.jpg');
  skyer2.hide();
  bjergproces=createImg('vaerker/bjergproces.png');
  bjergproces.hide();
  bjergglasur=createImg('vaerker/bjergglasur.jpg');
  bjergglasur.hide();
  bjerg=createImg('vaerker/bjerg.png');
  bjerg.hide();
  bjerg3d1=createImg('vaerker/bjerg3d1.png');
  bjerg3d1.hide();
  lampeblaa=createImg('vaerker/lampeblaa.jpg');
  lampeblaa.hide();
  lampehvid1=createImg('vaerker/lampehvid1.jpg');
  lampehvid1.hide();
  lampeblaa1=createImg('vaerker/lampeblaa1.png');
  lampeblaa1.hide();
  lampehvid=createImg('vaerker/lampehvid.jpg');
  lampehvid.hide();
  skaale=createImg('vaerker/skaale.jpg');
  skaale.hide();
  isbjoern=createImg('vaerker/isbjoern.jpg');
  isbjoern.hide();
  holdere=createImg('vaerker/holdere.jpg');
  holdere.hide();
  hjerne=createImg('vaerker/hjerne.jpg');
  hjerne.hide();
  fed=createImg('vaerker/fed.jpg');
  fed.hide();
  hund=createImg('vaerker/hund.jpg');
  hund.hide();
  isbjoern1=createImg('vaerker/isbjoern1.jpg');
  isbjoern1.hide();
  isbjoern2=createImg('vaerker/isbjoern2.jpg');
  isbjoern2.hide();
  hjerne1=createImg('vaerker/hjerne1.jpg');
  hjerne1.hide();
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(171,222,249);

  cloud=createImg('numbos.png');
  cloud.position(130,-25);
  cloud.size(300,200);
  textSize(30);
  stroke(0);
  strokeWeight(2);
  noFill();
  textFont('Helvetica');
  text('Cecilie Julie Holst Christensen',50,100);

  vaseknap=createButton('Graal-vase');
  vaseknap.position(50,350);
  vaseknap.style('background-color','#ABDEF9');
  vaseknap.style('border','2px solid #005668');
  vaseknap.style('font-size','20px');
  vaseknap.mousePressed(vase);

  drikkeglasknap=createButton('Drikkeglas');
  drikkeglasknap.position(50,390);
  drikkeglasknap.style('background-color','#ABDEF9');
  drikkeglasknap.style('border','2px solid #005668');
  drikkeglasknap.style('font-size','20px');
  drikkeglasknap.mousePressed(drikkeglas);

  kopperknap=createButton('Kopper');
  kopperknap.position(50,430);
  kopperknap.style('background-color','#ABDEF9');
  kopperknap.style('border','2px solid #005668');
  kopperknap.style('font-size','20px');
  kopperknap.mousePressed(kop);

  skyknap=createButton('Skyer');
  skyknap.position(50,470);
  skyknap.style('background-color','#ABDEF9');
  skyknap.style('border','2px solid #005668');
  skyknap.style('font-size','20px');
  skyknap.mousePressed(skyerne);

  bjergknap=createButton('Bjerg');
  bjergknap.position(50,510);
  bjergknap.style('background-color','#ABDEF9');
  bjergknap.style('border','2px solid #005668');
  bjergknap.style('font-size','20px');
  bjergknap.mousePressed(bjergi);

  lampeknap=createButton('Glaslamper');
  lampeknap.position(50,550);
  lampeknap.style('background-color','#ABDEF9');
  lampeknap.style('border','2px solid #005668');
  lampeknap.style('font-size','20px');
  lampeknap.mousePressed(lampe);

  brugsknap=createButton('Andet brugskunst');
  brugsknap.position(50,590);
  brugsknap.style('background-color','#ABDEF9');
  brugsknap.style('border','2px solid #005668');
  brugsknap.style('font-size','20px');
  brugsknap.mousePressed(brugskunst);

  kontaktknap=createButton('Kontakt');
  kontaktknap.position(50,630);
  kontaktknap.style('background-color','#ABDEF9');
  kontaktknap.style('border','2px solid #005668');
  kontaktknap.style('font-size','20px');
  kontaktknap.mousePressed(kontakt);
}

function vase(){
  fill(171,222,249);
  rect(0,0,width,height);

  vaseimg.show();
  vaseimg.position(50,130);
  vaseimg.size(150,200);

  vaseimg1.show();
  vaseimg1.position(230,130);
  vaseimg1.size(150,200);

  vaseimg2.show();
  vaseimg2.position(410,130);
  vaseimg2.size(150,200);

  textSize(14);
  noStroke();
  fill(0);
  text('Pustet glasvase',570,150);
  text('Graal teknik',570,170);
  text('Grønne pulverblade på brun solid',570,190);

  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();
}

function drikkeglas(){
  fill(171,222,249);
  rect(0,0,width,height);

  hoeje.show();
  hoeje.position(50,130);
  hoeje.size(150,200);

  lilglas.show();
  lilglas.position(230,130);
  lilglas.size(150,200);
  lilglas.mousePressed(lil1);
  textSize(12);
  noStroke();
  fill(0);
  text('Tryk for flere billeder',230,340);

  striber.show();
  striber.position(410,130);
  striber.size(150,200);
  striber.mousePressed(strib);
  text('Tryk for flere billeder',410,340);

  vinglas.show();
  vinglas.position(590,130);
  vinglas.size(150,200);

  fill(0)
  text('Høje vandglas. Pustede',850,150);
  text('Lille vandglas med fod. Pustet',850,170);
  text('Serie af stribede glas. Pustede med afmærkningsform',850,190);
  text('Ølglas, vinglas, snapseglas 3 str. Pustede',850,210);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function lil1(){
  lilglas1.show();
  lilglas1.position(230,130);
  lilglas1.size(150,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  lilglas.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();
}

function strib(){
  striber1.show();
  striber1.position(410,130);
  striber1.size(250,200);
  striber1.mousePressed(strib2);
  if(striber.mousePressed){
  vinglas.position(690,130);
  }

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function strib2(){
  striber2.show();
  striber2.position(410,130);
  striber2.size(250,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function kop(){
  fill(171,222,249);
  rect(0,0,width,height);

  avokopper.show();
  avokopper.position(50,130);
  avokopper.size(150,200);
  kopper.show();
  kopper.position(230,130);
  kopper.size(150,200);

  textSize(14);
  noStroke();
  fill(0);
  text('Drejede kopper',390,150);
  text('Saltler og Ler med lavaplet',390,170);
  text('Glasur. Dyppede',390,190);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function skyerne(){
  fill(171,222,249);
  rect(0,0,width,height);

  sky.show();
  sky.position(50,130);
  sky.size(150,200);
  skyer1.show();
  skyer1.position(410,130);
  skyer1.size(150,200);
  textSize(12);
  noStroke();
  fill(0);
  text('Tryk for flere billeder',410,340);
  skyer1.mousePressed(skyetc)
  skyer.show();
  skyer.position(230,130);
  skyer.size(150,200);
  skyerbord.show();
  skyerbord.position(590,130);
  skyerbord.size(250,200);

  textSize(14);
  noStroke();
  fill(0);
  text('Pustede glasskyer',950,150);
  text('Formpustede i to str forme',950,170);
  text('Pulverfarver',950,190);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function skyetc(){
  skyer3.show();
  skyer3.position(410,130);
  skyer3.size(150,200);
  skyer3.mousePressed(skyosv);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function skyosv(){
  skyer2.show();
  skyer2.position(410,130);
  skyer2.size(250,200);
  if(skyer1.mousePressed){
  skyerbord.position(690,130);
  }

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function bjergi(){
  fill(171,222,249);
  rect(0,0,width,height);

  bjergproces.show();
  bjergproces.position(50,130);
  bjergproces.size(150,200);
  bjergglasur.show();
  bjergglasur.position(230,130);
  bjergglasur.size(150,200);
  bjerg.show();
  bjerg.position(410,130);
  bjerg.size(150,200);
  bjerg3d1.show();
  bjerg3d1.position(590,130);
  bjerg3d1.size(150,200);

  textSize(14);
  noStroke();
  fill(0);
  text('Modeleret bjerg. Todelt krukke',800,150);
  text('Grå lavaplet-ler',800,170);
  text('Krympeglasur på låg',800,190);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function lampe(){
  fill(171,222,249);
  rect(0,0,width,height);

  lampeblaa.show();
  lampeblaa.position(50,130);
  lampeblaa.size(150,200);
  lampeblaa.mousePressed(blaa);
  textSize(12);
  noStroke();
  fill(0);
  text('Tryk for flere billeder',50,340);

  lampehvid1.show();
  lampehvid1.position(230,130);
  lampehvid1.size(150,200);
  lampehvid1.mousePressed(hvid);
  textSize(12);
  noStroke();
  fill(0);
  text('Tryk for flere billeder',230,340);

  textSize(14);
  noStroke();
  fill(0);
  text('Pustede lampe-"skærme"',390,150);
  text('Graal teknik',390,170);
  text('Blå solid med hvid pulver. Hvid solid med blå pulver',390,190);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function blaa(){
  lampeblaa1.show();
  lampeblaa1.position(50,130);
  lampeblaa1.size(150,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function hvid(){
  lampehvid.show();
  lampehvid.position(230,130);
  lampehvid.size(150,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();

}

function brugskunst(){
  fill(171,222,249);
  rect(0,0,width,height);

  skaale.show();
  skaale.position(50,130);
  skaale.size(150,200);

  isbjoern.show();
  isbjoern.position(230,130);
  isbjoern.size(150,200);
  isbjoern.mousePressed(bjoern);
  textSize(12);
  noStroke();
  fill(0);
  text('Tryk for flere billeder',230,340);

  holdere.show();
  holdere.position(410,130);
  holdere.size(150,200);

  hjerne.show();
  hjerne.position(590,130);
  hjerne.size(150,200);
  hjerne.mousePressed(hjernemasse);
  text('Tryk for flere billeder',590,340);

  fed.show();
  fed.position(770,130);
  fed.size(150,200);

  hund.show();
  hund.position(950,130);
  hund.size(150,200);

  fill(0)
  text('Drejede Skåle. Lavaplet-ler. Glasur: dyppede',1110,150);
  text('Modeleret isbjørneaskebæger / isbjørnekrukke.',1110,170);
  text('Grå lavaplet-ler. Begitning samt klar glasur',1110,190)
  text('Små drejede skåle. Rakubrændt saltler. Kobberglasur',1110,210);
  text('Modeleret hjernekrukke. Sort ler.',1110,230);
  text('Begitning samt klar glasur',1110,250);
  text('Drejet Krukke, trukkede arme, modeleret låg.',1110,270);
  text('Saltler. Glasur: Dyppet (i låg: Bobleglasur)',1110,290);
  text('Modeleret hundeaskebæger. Grå lavaplet-ler.',1110,310);
  text('Begitning og klar glasur',1110,330);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
}

function bjoern(){
  isbjoern1.show();
  isbjoern1.position(230,130);
  isbjoern1.size(150,200);
  isbjoern1.mousePressed(bjoern2)

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
}

function bjoern2(){
  isbjoern2.show();
  isbjoern2.position(230,130);
  isbjoern2.size(150,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
}

function hjernemasse(){
  hjerne1.show();
  hjerne1.position(590,130);
  hjerne1.size(150,200);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
}

function kontakt(){
  fill(171,222,249);
  rect(0,0,width,height);

  textSize(14);
  noStroke();
  fill(230,0,230);
  text('Cecilie Julie Holst Christensen', 50,140);
  text('cecilie-julie@hotmail.com', 50,160);
  text('21 28 23 04', 50,180);
  text('Instagram: rejerisotto', 50,200);
  text('Spotify:      pomelofrugt', 50,220);
  text('Vimeo:       pomelofrugt', 50,240);
  text('Gitlab:        pomelofrugter', 50,260);

  vaseimg.hide();
  vaseimg1.hide();
  vaseimg2.hide();
  hoeje.hide();
  lilglas.hide();
  striber.hide();
  vinglas.hide();
  lilglas1.hide();
  striber1.hide();
  striber2.hide();
  avokopper.hide();
  kopper.hide();
  sky.hide();
  skyer1.hide();
  skyer.hide();
  skyerbord.hide();
  skyer3.hide();
  skyer2.hide();
  bjergproces.hide();
  bjergglasur.hide();
  bjerg.hide();
  bjerg3d1.hide();
  lampeblaa.hide();
  lampehvid1.hide();
  lampeblaa1.hide();
  lampehvid.hide();
  skaale.hide();
  isbjoern.hide();
  holdere.hide();
  hjerne.hide();
  fed.hide();
  hund.hide();
  isbjoern1.hide();
  isbjoern2.hide();
  hjerne1.hide();
}

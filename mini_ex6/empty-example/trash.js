let bottle;
let banana;
let cigaret;
let button;
let begin;
let can=new Can;
let ice=new Ice;
let arrows;
let how;
let collectibles;

function setup(){
  createCanvas(windowWidth,windowHeight);

  let bottleimg=loadImage('bottle.png');
  bottle=createSprite(random(0,width),-35);
  bottle.addImage(bottleimg);
  let bananaimg=loadImage('banana.png');
  banana=createSprite(random(0,width),-30);
  banana.addImage(bananaimg);
  let cigaretimg=loadImage('cigaret.png');
  cigaret=createSprite(random(0,width),-30);
  cigaret.addImage(cigaretimg);
  let canimg=loadImage('can.png');
  can=createSprite(this.x,this.y,80,130);
  can.addImage(canimg);
  can.setCollider('rectangle',0,26,75,100);

  button=createButton('Press to start');
  button.position(585,200);
  button.style('background','#646464');
  button.style('border','none');
  button.style('color','#FFFFFF');
  button.style('padding','30px 33px');
  button.mousePressed(beginGame);

  collectibles=new Group;
  collectibles.add(bottle);
  collectibles.add(banana);
  collectibles.add(cigaret);
}

function draw(){
  background(0,130,255);

  setInterval(bottly,random(1000,3000));
  setInterval(baninis,random(1000,3000));
  setInterval(cigar,random(1000,3000));

  ice.show();
  can.overlap(collectibles, collect);
  drawSprites();
  if(keyIsDown(LEFT_ARROW)){
    can.move-5;
  }else if(keyIsDown(RIGHT_ARROW)){
    can.move();
  }
}

function beginGame(){
  //hiding button and text from before
  button.hide();

  //next explanation
  how=createDiv('Catch the trash, save your earth!');
  how.style('color','#000000');
  how.position(400,50);
  how.style('font-size','30px');
  arrows=createDiv('<--  -->')
  arrows.style('color','#000000');
  arrows.position(550,100);
  arrows.style('font-size','30px');
}

function bottly(){
  bottle.velocity.y=random(0,5);
  if(bottle.position.y>height){
    bottle.position.y=-100;
    bottle.position.x=random(0,width);
  }
}

function baninis(){
  banana.velocity.y=random(0,5);
  if(banana.position.y>height){
    banana.position.y=-100;
    banana.position.x=random(0,width);
  }
}

function cigar(){
  cigaret.velocity.y=random(0,1);
  if(cigaret.position.y>height){
    cigaret.position.y=-100;
    cigaret.position.x=random(0,width);
  }
}

function collect(can,collected){
  collectibles.remove();
}

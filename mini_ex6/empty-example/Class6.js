class Ice{
  constructor(){
    this.x=465;
    this.y=631;
    this.size=300;
  }
  win(){
    this.size=this.size+5;
    this.x=this.x-2
  }
  lose(){
    this.size=this.size-5;
    this.x=this.x+2
  }
  show(){
    fill(255);
    stroke(130);
    rect(this.x,this.y,this.size,20);
  }
}

class Can{
  constructor(){
    this.x=630
    this.y=560
  }
  move(){
      this.x=this.x+5
  }
  view(){
    rect(this.x,this.y,80,130);
    can=createSprite(this.x,this.y,80,130);
    can.addImage(canimg);
    can.setCollider('rectangle',0,26,75,100);
  }
}

**A Game?**

*Take the crap that programming throwes at you*  
https://glcdn.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex6/empty-example/indexworking6.html  
(A program that does **something**)  
![screenshot](sk--rmbillede-2019-03-18-kl--1.png)  

*Save your Earth - THE GAME*  
https://glcdn.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex6/empty-example/index6.html  
(A program that looks slightly better, but does not work)  
![screenshot](sk--rmbillede-2019-03-18-kl--1_1_.png)  

This week the weekly frustration, that normally keeps me going, turned into anger and desperation.  
I quickly got an idea for a game: Save the Earth - THE GAME. Unfortunately my capabilities did not match my vision..  
I spend my entire week trying to get Save your Earth to work, but with no luck.  
I wanted it to be a trash can that catched all the trash.  
Whenever you catched something the iceblock under the can would expand, being a symbol of cleaning up the Earth.
If you did not catch it, the iceblock should shrink, global warming style.

I used the p5.PLAY library to make the trash into collectible items. 
Using the overlap function with the can-sprite, I wanted to collect one piece of trash at a time.  
I used keyPressed(LEFT_ARROW,RIGHT_ARROW) to move the can from side to side.
But none of this worked - and my can ended in the top left corner :) ?????????  
I used the setInterval for the trash, so that it would fall in a pace that was managable.  

Several times I tried changing the code, trying without the p5.PLAY and at some point I even tried copying Annebel's code, only changing out the pictures.
Nothing came up on my html-page, and no error was shown in the console.  
This broke my temper.  

So the other program I have made reflects this overwhelming frustration. It is a game where you have to give up.  
I used Winnie's simplified code and modified it slightly. The emoji has no choice but to take the crap. 
You cannot stop the crap from coming. And you cannot move away from it even though the program tells you to do so.
Everytime you press the mouse more crap just drops from the sky.  
Hopefully this will frustrate you :)))))))
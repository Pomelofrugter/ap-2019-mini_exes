**#Waltz of the words // Amia and Cecilie**

https://glcdn.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex8/empty-example/index8.html  
(Try pressing your mouse)

![screenshot](flowers.png)
![screenshot](plants.png)

Amia and I got our idea for this weeks program from the JSON-example with animals that group six had made for this week's class. 
You can say the our idea "grew" out of it. 
We tried to make a JSON-file and load it in our JS-file, but it kept calling an error that our variable was not defined, and we could not figure the reason out.
So in the end we ended the JSON-experiment and kept the plants and flowers in arrays. 
We used for-loops and 2D-arrays to practice these syntaxes. 
We also wanted to add sounds to the two different "states" we created (the flower-state and the plant-state).
For Flowers we used the melody 'Waltz of the flowers' by Tchaikovsky. 
If the mouse is pressed and hold the "state" changes to Plants.
For Plants we used this [youtube-video's](https://www.youtube.com/watch?v=L0cOEuLbLrc&t=7s) sound:  
For some reason the songs plays differently in firefox and chrome (and not at all in safari hmm).
But [here](https://youtu.be/S5xau561fv4) is a video of it in use.  

Regarding the movement of the words, our first idea was to set the arrays to shift randomly, but then we changed the "j's" starting point in our for-loops to be 0.5, which for some reason made the words shift with every frame.
Maybe the reason for this shift is that it instead of drawing one every frame draws only a half?  
But then again there is a connection to the amount of rows / cols and the amount of possibilities in the arrays.
This is something I will ask Winnie about!  

In the text 'Vocable Code' in the book *Speaking Code* by George Cox and Alex McLean they write: 
*The referent “the sentence” is understandable only within the overall context of the words that make a sentence, this being one of the key structural elements of written language as a whole.* - p. 20  
We wanted to challenge this by creating a program that looks like it consists of a lot of sentences (because of the bigger space between the words on the Y-axes, which hopefully makes it look like lines.)  
But this is not the case: It is merely words of different plants.
Because of the constant change of flowers (and with mouseIsPressed the change of plants), the recipient will probably be even more confused.
We also used the connotations between the soundtrack, the words and the emoji of a tulip and a leaf to try to examine what Cox and McLean means by: "It [the code] oscillates between written and spoken forms." - p. 18.  
Because as they write: Code is closer to the phonetic version of sentences because of the spelling and syntax you are able to create.
But by mixing sound and words we hope to make the oscillation even clearer.  
We also wanted to work with secondary notation, in the sense that we would change variable- and function-names (as we see it in e.g. Winnie Soon, *Vocable Code*).
But given it was our first time working colaborative we found out we needed to keep our code as easy and clean as possible.
The last conceptual decision was to keep the loop that the plant-sound creates.
This loop layers the same piece of the sound on top of each other the longer you keep pressing the mouse. 
This is caused by the function plants.play(); is called in draw.
Firstly we found it annoying, but I think it works as a nice comment to the piece *I am sitting in a room (for voice on tape)* (1969, Alvin Lucier).
Instead of recording the sound and recording the recording of that sound, we led the loop happen directly in our program.  

[See sourcecode here](https://gitlab.com/Pomelofrugter/ap-2019-mini_exes/blob/master/mini_ex8/empty-example/Class8.js)
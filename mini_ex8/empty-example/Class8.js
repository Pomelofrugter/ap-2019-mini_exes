//Created by Amia and Cecilie. Using Tchaikovsky - Waltz for the Flowers and youtubeclip:
//https://www.youtube.com/watch?v=L0cOEuLbLrc&t=7s

let flowers=['amaryllis','anemone','amaranth','azalea','tulip'];
let plants=['Alder','black alder','common alder','false alder','gray alder','speckled alder','white alder'];
let index=0;
let rows=20;
let cols=17;
let lol;
let flowersong;
let plantsong;

function preload(){
  flowersong=loadSound('WaltzFlowers.mp3')
  plantsong=loadSound('KillPlants.mp3')
}

function setup(){
  frameRate(1);
  createCanvas(windowWidth,windowHeight);
    flowersong.play();
}

function draw(){
  //"First" background: Flower-words
  background('#F7819F');
  //2D Array creating word-grid
  for(i=1;i<rows;i++){
    for(j=0.5;j<cols;j++){
      let it=i*70;
      let that=j*50;
      noFill();
      stroke(0);
      strokeWeight(0.8);
      textSize(12);
      textFont('American Typewriter');
      text(flowers[index],it,that);
      index++;
      if(index>=flowers.length){
        index=1;
      }
    }
  }
  //Emoji front
  textSize(200);
  text('🌷',615,400);

  //Press mouse to change background: Plant-words
  if(mouseIsPressed){
    flowersong.pause();
    background('#088A4B');
    //New grid of plants
    for(i=0.5;i<rows;i++){
      for(j=1;j<cols;j++){
        let it=i*80;
        let that=j*50;
        noFill();
        stroke(0);
        strokeWeight(0.8);
        textSize(12);
        textFont('American Typewriter');
        text(plants[index],it,that);
        index++;
        if(index>=plants.length){
          index=0;
        }
      }
    }
    //New emoji front
    textSize(200);
    text('🌿',615,400);
    //Play crazy plant lady
    plantsong.play();
  }
}
//Play Tchaikovsky again
function mouseReleased(){
  flowersong.play();
  plantsong.pause();
}

let myLove=['In loving thee thou know’st I am forsworn','But thou art twice forsworn to me love swearing','In act thy bed-vow broke and new faith torn','In vowing new hate after new love bearing','But why of two oaths breach do I accuse thee','When I break twenty? I am perjured most','For all my vows are oaths but to misuse thee','And all my honest faith in thee is lost','For I have sworn deep oaths of thy deep kindness','Oaths of thy love, thy truth, thy constancy','And, to enlighten thee, gave eyes to blindness','Or made them swear against the thing they see','  For I have sworn thee fair: more perjured eye','  To swear against the truth so foul a lie'];
let shakespeare=21;
let sonnet=25;
let be=0;
let myHeart=0;
let mySecrets;

function preload(){
  mySecrets=loadSound('152.m4a');
}

function setup() {
  frameRate(0.5);
  createCanvas(1438,730);
  mySecrets.setVolume(0.002);
  mySecrets.loop();
}

function draw() {
  background(232,238,232);

  fill(0,myHeart);
  textSize(50);
  text(myLove[be],250,350);
  if(be<myLove.length){
    be++;
    myHeart=myHeart+2;
    if(be>=myLove.length){
    be=0;
    }
  }

  for(nothing=0;nothing<shakespeare;nothing++){
    for(all=0;all<sonnet;all++){
      let it=nothing*70;
      let that=all*30;
      fill(255);
      textSize(8);
      text('Sonnet 152',it,that);
    }
  }
}

**Mini_ex10 - Flowcharts**  

*Individual*  
I have chosen to make a flowchart on my mini_ex7 because I found that weeks source code particurly hard to work with.  

In the flowchart I struggled with the division. 
How many parts to make and how to arrange the arrows. 
Especially with the decision parts. 
Should they point back to another field or should I make a new one.  
![screenshot](flowchart.png)  

[Link to mini_ex7](https://gitlab.com/Pomelofrugter/ap-2019-mini_exes/tree/master/mini_ex7)  

*Group*  
For the group assignment we agreed on going conceptually and politically with our ideas.  
Both group flowcharts focuses on the points in the texts from data capture. 
We desided to work with ideas for critical designs.  
Flowchart 2A is inspired by two different artefacts:  
- [this](https://thegirlshop.dk/?fbclid=IwAR27Bz1DetK1GZQD_3dPbZ6CXJ0p3QfPmGz4pmPnY21AlollK276eK6ZXn8) "webshop" made by Danmarks Indsamlingen  
- and the possibility of doing a fundraising of ones own choice on facebook on your birthday.  

Of course it is a good idea to raise money for important causes, but the facebook fundraising almost becomes a part of the like-economy, and we wanted to adress this.  

Flowchart 2B is inspired specifically by the the text 'The like economy: Social buttons and the data-intensive web' and especially the sentence: *"Facebook has been discussed and criticised as a walled garden (Berners-Lee, 2010)*".  

![screenshot](flowchart2A.jpg)  
*Flowchart 2A*  
Technically it could be diffucult to balance all the different ideas we get, and especially to connect the ranking to something like a number or a slider.  


![screenshot](flowchart2B.jpg)  
*Flowchart 2B*  
Technically it could be difficult to "draw" a landscape in code, if we want to make a wall, flowers etc.  

When we first met in the group we were a bit frustrated, having no idea how to make a flowchart when we was not sure what we wanted to make for our final exam. 
But as seen in this week's (and last week's) reading flowcharts are not necesarily a structuring tool. 
It worked nicely for us to use it as a brainstorming tool, and we ended up having loads of papers with different flowcharts and loads of different ideas.  
The really big different between these charts and the individual chart is that I looked at trying to describe the technical aspects of my program in simpler turns, while the group charts are build on conceptual ideas, and we did not think too much of the technical aspects.  

George Cox writes in his text 'Machine Ways of Seeing':  
*"That machines learn is accounted for through a combination of calculative practices that help to approximate what will likely happen through the use of different algorithms and models."*   
And I think that we can use flowcharts in this way; by dividing our ideas of programs into different parts we can calculate what is technically needed and what the outcome of different actions will be. 
In this way we can almost use flowcharts as algorothims.


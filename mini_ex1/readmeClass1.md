![ScreenShot](Planet.png)

https://cdn.staticaly.com/gl/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex1/empty-example/index.html

I started out wanting to make a planet with a ring - like Saturn. 
I used WEBGL to be able to make the torus / ring rotate around the sphere / planet.
Then the piece evolved from there.
I played around with different syntax wanting, still, to keep it as pictorial as possible. 
Frustration came to me, and what I deliver now feels unfinished to me.
But I can't learn everything at the same time. 
In this first mini_ex I tested different variables for color and coordinates.
I used the random function both to make a couple of my "stars" / circles blink in different colours as to make it look like chromatic aberration. 
The random function was also used to make a star "shoot around" or simulate blinking stars. 
But it moves to fast for my liking.
    - This I wan't to investigate for my next mini_ex: How to maybe lower the framecount or something, so that a randomly moving objects slows down.
    Also I had the idea that using the random function would make it look like it generated new stars all over the place, but here I ran into two problems:
        1. Then I had to move the background up under setup, which would cause the torus (Saturns ring) to leave a trace of the light blue whenever it rotated.
        2. And then I would also have to figure out how to use noLoop or something similiar for stopping the randomly moving stars / circles from filling up the entire screen. 
        Or maybe a variable that would reverse the movement, making the stars disappear again.
        I have the feeling that I am going to need 'if-else' statements for the above, and I just cannot understand them yet.
I removed the cursor so to make it some sort of visuel piece instead of just an image.
All the small circles or stars are created severally because I could not find a varible that made it possible to generate multiple circles with different coordinates. 
This is also a piece of code I am going to look for in my future mini_exs :)
Lastly I framed my galaxy-image with to unfilled beziers, hoping for it to give milkyway connotations. 

I think I want to do something interactive with my next mini_ex, e.g. using the mousePressed function.
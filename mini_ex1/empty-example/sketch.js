let yPos = 12;
function setup() {
  createCanvas(640,480, WEBGL);
  frameRate(30);
}
function draw() {
  background(random(1), random(100), random(1000));
  yPos = yPos - 100;
  if (yPos < 20) {
    yPos = height;
    }
  let c = color(100,100,1000);
  fill(c);
  square(200, yPos, width, yPos);
  c = color(200,20,4000);
  fill(c);
  sphere(40);
  rect(-300, 100, 60, 60);
  arc(50, 55, 50, 50, 0, HALF_PI);
  noFill();
  arc(50, 55, 60, 60, HALF_PI, PI);
  arc(50, 55, 70, 70, PI, PI + QUARTER_PI);
  arc(50, 55, 80, 80, PI + QUARTER_PI, TWO_PI);
}
